package main

import (
	"context"
	"log"

	"github.com/micro/go-micro"
	proto "gitlab.com/jozuenoon/weather_station/proto"
)

type Greeter struct{}

func (g *Greeter) Hello(ctx context.Context, req *proto.HelloRequest, rsp *proto.HelloResponse) error {
	rsp.Greeting = "Hello " + req.Name
	return nil
}

func main() {
	service := micro.NewService(
		micro.Name("greeter"),
	)

	service.Init()

	err := proto.RegisterGreeterHandler(service.Server(), new(Greeter))
	if err != nil {
		panic(err)
	}

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
