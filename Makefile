SHELL := /bin/bash

in := $(shell find proto -name '*.proto')
out := $(patsubst %.proto,%.pb.go,$(in))

protos: $(out)

%.pb.go: %.proto
	protoc --proto_path=$$GOPATH/src:. -I=./proto --micro_out=. --go_out=. $<
