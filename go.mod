module gitlab.com/jozuenoon/weather_station

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.8.2
	github.com/nats-io/nats-server/v2 v2.0.2 // indirect
	gitlab.com/gitlab-org/project-templates/go-micro v0.0.0-20190225134054-1385fab987bc
)
